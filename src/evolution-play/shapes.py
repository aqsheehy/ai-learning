import itertools
import numpy as np
import random

def normalize_shapes(a, b):
    ''' 
    Takes a given shape and resizes it given the target shape.
    The input shape must maintain the same size i.e. (A, B, C) => Size == A * B * C    
    '''
    targetTotal = sum(b)
    expectedTotal = sum(a)

    if (targetTotal == expectedTotal):
        return b

    targetPercentages = map(lambda x: 100 * x / targetTotal, b)
    expectedShape = list(map(lambda x: int(x / 100 * expectedTotal), targetPercentages))

    # Sums must equal
    output = list(expectedShape)
    while (sum(output) != expectedTotal):
        index = random.randrange(len(output))
        if (expectedTotal - sum(output) > 0):
            output[index] += 1
        else:
            output[index] -= 1

    return output


def inconsistent_shape_difference(a, b):
    '''
    Returns a degree of difference between two shapes, 
    which increases as their arity separates further
    '''
    zipped = list(itertools.zip_longest(a, b))
    totalShapeDifference = np.sum([np.absolute(0 if a is None else a - 0 if b is None else b) for a, b in zipped])
    shapeWeightDifference = (np.absolute(len(a) - len(b)) + 1)
    return totalShapeDifference * shapeWeightDifference

def consistent_shape_difference(a, b):
    return np.sum(np.absolute(np.subtract(a, b)))