import random
import numpy as np
from deap import algorithms, tools
from primitives import generate_primitives
from toolbox import generate_toolbox
random.seed(318)

def main():
    pset = generate_primitives()

    input_shape = (11, )
    output_shape = (None, 8)
    toolbox = generate_toolbox(pset, input_shape, output_shape)

    pop = toolbox.population(n= 300)
    hof = tools.HallOfFame(1)
    
    stats_fit = tools.Statistics(lambda ind: ind.fitness.values)
    stats_size = tools.Statistics(len)
    mstats = tools.MultiStatistics(fitness=stats_fit, size=stats_size)
    mstats.register("avg", np.mean)
    mstats.register("std", np.std)
    mstats.register("min", np.min)
    mstats.register("max", np.max)

    pop, log = algorithms.eaSimple(pop, toolbox, 0.5, 0.1, 40, stats=mstats, halloffame=hof, verbose=True)
    generator = toolbox.compile(expr=hof[0])
    model = generator(input_shape)
    model.summary()

if __name__ == "__main__":
    main()