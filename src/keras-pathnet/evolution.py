import random
import multiprocessing
from deap import  algorithms, base, creator, tools
import numpy as np

class Evolution():
    def __init__(self, pathNet):
        self.net = pathNet
        (L, M, N) = self.net.shape
        creator.create("MinimizeLoss", base.Fitness, weights=(-1.0,))
        creator.create("Genotype", np.ndarray, fitness=creator.MinimizeLoss)

        toolbox = base.Toolbox()
        toolbox.register("genotype", self.genotype, creator.Genotype)
        toolbox.register("population", tools.initRepeat, list, toolbox.genotype)

        toolbox.register("select", tools.selTournament, tournsize=2)
        toolbox.register("evaluate", self.net.evaluate)
        toolbox.register("mate", self.mate)
        toolbox.register("mutate", self.mutate)
        
        pool = multiprocessing.Pool(processes=2)
        toolbox.register("map", pool.map)

        self.toolbox = toolbox

    def genotype(self, indFn):
        '''
        Generator function required by DEAP for individual generation
        '''
        (L, M, N) = self.net.shape
        return indFn(np.random.randint(0, M, (L,N)))

    def population(self, num):
        '''
        Generates a population to be selected from 
        '''
        return self.toolbox.population(n = num)

    def mutate(self, path):
        '''
        Randomly updates the selected roads of the given path
        '''
        (L, M, N) = self.net.shape
        for i in range(L):
            for j in range(N):
                if random.random() < 1.0 / (L * N):
                    gene = path[i][j] + random.randint(-2, 2)
                    if gene < 0:
                        gene += M
                    elif gene > M - 1:
                        gene -= M
                    path[i][j] = gene
        return path,

    def mate(self, a, b):
        '''
        Randomly crosses road selections between a & b
        '''
        (L, M, N) = self.net.shape
        for i in range(L):
            for j in range(N):
                if random.randint(0, 9) < 5:
                    a[i][j] = b[i][j]
                else:
                    b[i][j] = a[i][j]
        return a, b

    def selectFrom(self, population):
        '''
        Selects the fittest path from the given population
        '''
        hof = tools.HallOfFame(1)
    
        stats_fit = tools.Statistics(lambda ind: ind.fitness.values)
        stats_fit.register("avg", np.mean)
        stats_fit.register("std", np.std)
        stats_fit.register("min", np.min)
        stats_fit.register("max", np.max)

        population, log = algorithms.eaSimple(population, self.toolbox, 0.5, 0.1, 40, stats=stats_fit, halloffame=hof, verbose=True)
        return (population, log, hof)