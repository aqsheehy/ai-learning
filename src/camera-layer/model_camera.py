import keras
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Input, Concatenate
from keras import backend as K

def model(input_shape, num_classes):

    inputs = Input(shape=input_shape)
    input_parser = Conv2D(32, kernel_size=(3, 3), activation='relu')(inputs)
    input_parser = MaxPooling2D(pool_size=(2, 2))(input_parser)

    camera1 = camera( input_parser )
    camera2 = camera( input_parser )
    camera3 = camera( input_parser )

    merged = Concatenate(axis=-1)([ camera1, camera2, camera3 ])

    flattened = Flatten()(merged)
    output = Dense(num_classes, activation='softmax')(flattened)

    model = Model(inputs=inputs, outputs=output)

    model.compile(loss=keras.losses.categorical_crossentropy,
        optimizer=keras.optimizers.Adadelta(),
        metrics=['accuracy'])

    return model

def camera(input_features, camera_size = 5 * 5, conv_units = 3, kernel_size = (3, 3), pool_size= (2, 2)):
    camera1 = Dense( camera_size )(input_features)
    camera1 = Conv2D( conv_units, kernel_size = kernel_size )(camera1)
    camera1 = MaxPooling2D(pool_size = pool_size)(camera1)
    return camera1