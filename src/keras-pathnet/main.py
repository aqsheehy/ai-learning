from evolution import Evolution
from pathnet import PathNet, PathLayer, PathModule
from keras.layers import Input, Conv2D, Dense, Activation, add, Flatten
import mnist

def main():
    '''
    L = # of Layers within the overall PathNet
    M = # of Modules per Layer
    N = # of Modules Paths can choose from each layer
    '''
    (input_shape, x_train, x_test, y_train, y_test) = mnist.prepare_mnist()

    num_paths = 64
    shape = (L, M, N) = (3, 10, 3)
    modules = lambda: list(list(map(lambda _: PathModule(add, [ Dense(20), Activation('relu') ]), [None] * M)))
    layers = list(list(map(lambda _: PathLayer( modules() ), [None] * L)))

    output = PathModule(add, [ Flatten(), Dense(10), Activation('softmax') ])
    pn = PathNet( layers, shape, [ Input(shape = input_shape) ], [output] )
    ev = Evolution(pn)

    pn.setData(x_train, x_test, y_train, y_test)

    # input first dataset
    pop = ev.population(num_paths)
    (pop, log, hof) = ev.selectFrom(pop)
    pn.fix(hof[0])

    # input second dataset
    #pop = ev.population()
    #(pop, log, hof) = ev.selectFrom(pop)
    #pn.fix(hof[0])

if __name__ == '__main__':
    # freeze_support() here if program needs to be frozen
    main() 