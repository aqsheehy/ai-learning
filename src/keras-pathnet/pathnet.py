import keras
from keras.models import Model

class PathModule():
    '''
    Each PathNet layer consists of M modules.
    '''
    def __init__(self, merge, nn):
        self.merge = merge
        self.nn = nn

    def connect(self, layers):
        output = layers[0] if len(layers) == 1 else self.merge(layers)
        for node in self.nn:
            output = node(output)
        return output

    def fix(self):
        # TODO Support disabling a module
        return

class PathLayer():
    '''
    Each PathNet consists of L layers.
    '''
    def __init__(self, modules):
        self.modules = modules

    def connect(self, layers, layerPath):
        return [ self.modules[i].connect(layers) for i in layerPath ]

    def fix(self):
        # TODO Support disabling a layer
        return

class PathNet():
    '''
    Represents the full pathnet network
    '''
    def __init__(self, layers, shape, inputs, outputs):
        #self.layers = layers
        self.shape = shape
        #self.inputs = inputs
        #self.outputs = outputs
        self.hi = 'yo'

    def connect(self, path):
        '''(L, M, N) = self.shape
        connected = self.inputs
        for i in range(L):
            connected = self.layers[i].connect(connected, path[i])
        outputs = [ i.connect(connected) for i in self.outputs ]
        return Model( inputs = self.inputs, outputs = outputs )'''
        return None

    def evaluate(self, path):
        model = self.connect(path)

        model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.SGD(lr=0.0001, decay=1e-6, momentum=0.9, nesterov=True),
              metrics=['accuracy'])

        (x_train, x_test, y_train, y_test) = self.data
        model.fit(x_train, y_train, batch_size=16, epochs=3, verbose=1, validation_data=(x_test, y_test))
        score = model.evaluate(x_test, y_test, verbose=0)
        return (score[1], )

    def setData(self, x_train, x_test, y_train, y_test):
        self.data = (x_train, x_test, y_train, y_test)

    def fix(self, path):
        return True