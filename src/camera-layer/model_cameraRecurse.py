import keras
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Input, Concatenate
from keras import backend as K

def model(input_shape, num_classes):

    conv_units = 32
    kernel_size = (3, 3)

    inputs = Input(shape=input_shape)
    input_parser = Conv2D(conv_units, kernel_size = kernel_size, padding='same', activation='relu')(inputs)
    input_parser = MaxPooling2D(pool_size=(2, 2))(input_parser)

    camera1 = camera( input_parser )
    camera2 = camera( Concatenate(axis=-1)([input_parser, camera1]) )
    camera3 = camera( Concatenate(axis=-1)([input_parser, camera1, camera2]) )

    merged = Concatenate(axis=-1)([ camera1, camera2, camera3 ])
    merged = MaxPooling2D(pool_size=(2, 2))(merged)
    merged = Flatten()(merged)
    merged = Dropout(0.5)(merged)
    output = Dense(num_classes, activation='softmax')(merged)

    model = Model(inputs=inputs, outputs=output)

    model.compile(loss=keras.losses.categorical_crossentropy,
        optimizer=keras.optimizers.Adadelta(),
        metrics=['accuracy'])

    return model

def camera(input_features, camera_size = 10, conv_units = 32, kernel_size = (3, 3)):
    camera1 = Dense( camera_size )(input_features)
    camera1 = Conv2D( conv_units, kernel_size = kernel_size, padding='same' )(camera1)
    camera1 = Dropout(0.25)(camera1)
    return camera1