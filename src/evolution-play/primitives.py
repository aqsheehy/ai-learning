import operator
import string
import random 
from deap import algorithms, base, creator, tools, gp
from keras.models import Model, Sequential
from keras.layers import Dense, Input, Reshape, Activation, Layer, Add
import shapes

INVALIDATOR = 1000000000

def generate_primitives():
    pset = gp.PrimitiveSetTyped("MAIN", [tuple], Model)
    pset.addPrimitive(add_input, [tuple], Node, name="Input")

    pset.addPrimitive(add_dense, [int, Node], Node, name="Dense")

    # See comment on why this is unavailable right now
    # pset.addPrimitive(add_reshape, [tuple, Node], Node, name="Reshape")

    # Resizing issues causing a problem, shapes must be compatible
    # pset.addPrimitive(add_addition, [Node, Node], Node, name='Add')

    pset.addPrimitive(add_elu, [Node], Node, name="ELU")
    pset.addPrimitive(add_softmax, [Node], Node, name="SoftMax")
    pset.addPrimitive(add_tanh, [Node], Node, name="Tanh")
    pset.addPrimitive(add_sigmoid, [Node], Node, name="Sigmoid")

    pset.addPrimitive(return_model, [Node], Model, name="Model")

    pset.addTerminal(add_input((INVALIDATOR,)), Node, name="TerminalLayer")
    pset.addPrimitive(pass_through, [Node], Node, name="PassthroughLayer")

    pset.addEphemeralConstant('UnitCount', lambda: random.randint(1,64), int)
    pset.addPrimitive(pass_through, [int], int, name="PassthroughInteger")

    pset.addPrimitive(pass_through, [tuple], tuple, name="PassthroughTuple")
    
    pset.addTerminal(return_model(add_input((INVALIDATOR,))), Model, name="TerminalModel")
    pset.addPrimitive(pass_through, [Model], Model, name="PassthroughModel")
    
    return pset

def add_input(input_shape):
    output_layer = Input(input_shape)
    return Node([], output_layer)

def add_dense(units, input_node):
    output_layer = Dense(units)(input_node.layer)
    return Node([input_node], output_layer)

def add_reshape(output_shape, input_node):
    # TODO Tony Sheehy - Expecting numpy shape, getting tensorflow.python.framework.tensor_shape.Dimension
    output_shape = shapes.normalize_shapes(input_node.layer.shape, output_shape)
    output_layer = Reshape(output_shape)(input_node.layer)
    return Node([input_node], output_layer)

def add_elu(input_node):
    output_layer = Activation('elu')(input_node.layer)
    return Node([input_node], output_layer)

def add_softmax(input_node):
    output_layer = Activation('softmax')(input_node.layer)
    return Node([input_node], output_layer)

def add_tanh(input_node):
    output_layer = Activation('tanh')(input_node.layer)
    return Node([input_node], output_layer)

def add_sigmoid(input_node):
    output_layer = Activation('sigmoid')(input_node.layer)
    return Node([input_node], output_layer)

def add_addition(node_one, node_two):
    output_layer = Add()([node_one.layer, node_two.layer])
    return Node([node_one, node_two], output_layer)

def pass_through(input_node):
    return input_node

def return_model(output_node):
    input_layers = [i.layer for i in get_input_nodes(output_node)] 
    output_layer = Dense(INVALIDATOR)(output_node.layer) if output_node.origin else output_node.layer
    return Model(inputs = input_layers, outputs=output_layer)

def get_input_nodes(output_layer):
    if (output_layer.origin): 
        return [ output_layer ]
    node_inputs = [i for i in output_layer.inputs if i.origin]
    parent_inputs = sum([ get_input_nodes(i) for i in output_layer.inputs ], []) #flatten
    all_inputs = node_inputs + parent_inputs
    return list(set(all_inputs)) #distinct

class Node():
    def __init__(self, inputs, layer):
        self.origin = len(inputs) == 0
        self.inputs = inputs
        self.layer = layer